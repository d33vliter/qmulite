
# QMULITE

Script para ejecutar maquinas virtuales con QEMU-KVM.


## Instalacion:

Ejecutar `./instalar` (pedira contraseña)

Ya podras ejecutar QMULITE en tu terminal con `qmulite`


## Desinstalar:

Ejecutar `./desinstalar` (pedira contraseña)

---

[Descargar](https://gitlab.com/d33vliter/qmulite/-/archive/master/qmulite-master.zip)


**Captura:**

[![2020-01-14-210204-796x475-scrot.png](https://i.postimg.cc/pVDvcz7T/2020-01-14-210204-796x475-scrot.png)](https://postimg.cc/Lh6GnYmK)
